#define BOARD_HEIGHT 6
#define BOARD_WIDTH 8
#define SQUARE_SIZE 2.39f

#include <iostream>
#include <vector>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include "capture.h"

#include <alproxies/alvideodeviceproxy.h>
#include <alvision/alimage.h>
#include <alvision/alvisiondefinitions.h>
#include <alerror/alerror.h>


int width, height;
cv::Mat rt( 3, 4, CV_64F );
cv::Mat cameraMatrix( 3, 3, CV_64F );
cv::Mat rvec( 3, 1, CV_64F );


void read_conf() {
	cv::Mat rot_m( 3, 3, CV_64F );

	FILE *fp = fopen( "conf", "r" );
	if( fp == NULL ) {
		printf( "Error. Confifaili ei leitud.\n" );
		exit( 0 );
	}

	for( int i = 0; i < 3; i++ )
		fscanf( fp, "%lf %lf %lf\n", &cameraMatrix.at<double>(i, 0), &cameraMatrix.at<double>(i, 1), &cameraMatrix.at<double>(i, 2) );
	fscanf( fp, "%lf %lf %lf\n", &rvec.at<double>(0, 0), &rvec.at<double>(1, 0), &rvec.at<double>(2, 0) );
	fscanf( fp, "%lf %lf %lf", &rt.at<double>(0, 3), &rt.at<double>(1, 3), &rt.at<double>(2, 3) );

	cv::Rodrigues( rvec, rot_m );
	for( int i = 0; i < 3; i++ ) for( int j = 0; j < 3; j++ )
		rt.at<double>(i, j) = rot_m.at<double>(i, j);

	fclose( fp );
}


int main()
{
	IMAGE_CONTEXT *ic;
	XEvent event;
	cv::Mat frame;
	std::string clientName;
	AL::ALValue img;

	bool end = false;
	bool paus = false;


	// Init capture
	AL::ALVideoDeviceProxy camProxy( "192.168.1.103", 9559 );
	clientName = camProxy.subscribe("test", AL::kVGA, AL::kBGRColorSpace, 30);

	// Valime alumise kaamera
	camProxy.setParam( AL::kCameraSelectID, 1 );

	width = 640;
	height = 480;

	// Creating a new window
	ic = new_window( "Video", 100, 100, width, height );
	XImage *xImage1 = ic->xImage;
	unsigned char *imageLine1 = (unsigned char*) xImage1 -> data;

	read_conf();

	while( !end ) {

		const unsigned char *data;

		// Capturing the frame
		if( !paus ) {
			img = camProxy.getImageRemote( clientName );
			data =  static_cast<const unsigned char*> ( img[6].GetBinary() );
		}
		cv::Mat frame( height, width, CV_8UC3, (unsigned char*)data );


		cv::Mat res( 3, 1, CV_64F );
		cv::Mat coord = cv::Mat::ones( 4, 1, CV_64F );

		for( int i = 0; i > -300; i -= 10 ) {
			coord.at<double>(0, 0) = 0;
			coord.at<double>(1, 0) = i;

			res = cameraMatrix * rt * coord;
			res.at<double>(0, 0) /= res.at<double>(2, 0);
			res.at<double>(1, 0) /= res.at<double>(2, 0);
			res.at<double>(2, 0) = 1.0f;

			if( res.at<double>(0, 0) > 0 && res.at<double>(0, 0) < width && res.at<double>(1, 0) > 0 && res.at<double>(1, 0) < height )
				cv::circle( frame, cv::Point(res.at<double>(0, 0), res.at<double>(1, 0)), 3, cv::Scalar(0, 0, 255) );
		}
		


		// Showing the image
		int pix_c = 0;

		for( int j = 0; j < height; j++ ) {
			for( int i = 0; i < 3*width; i += 3 ) {
				imageLine1[ 4*pix_c + 0 ] = data[j*frame.cols*3 + i + 0 ];
				imageLine1[ 4*pix_c + 1 ] = data[j*frame.cols*3 + i + 1 ];
				imageLine1[ 4*pix_c + 2 ] = data[j*frame.cols*3 + i + 2 ];
				imageLine1[ 4*pix_c + 3 ] = 255;

				pix_c++;
			}
		}

		image_put( ic );
		usleep( 10000 );

		// Event handling
		if ( XPending( ic->display ) > 0 ) {
			XNextEvent( ic->display, &event );

			if( event.type == KeyPress ) {
				if( event.xkey.keycode == 9 ) {		// Esc was pressed
					camProxy.releaseImage( clientName );
					camProxy.unsubscribe( clientName );
		            end = true;
				}

				if( event.xkey.keycode == 65 )		// SPACE was pressed
					paus = !paus;
			}


			if( event.type == ButtonPress && paus ) {

				cv::Point2f p( event.xbutton.x, event.xbutton.y );
				cv::Mat screen_coord = ( cv::Mat_<double>(3, 1) << event.xbutton.x, event.xbutton.y, 1.0 );
				cv::circle( frame, p, 3, cv::Scalar(255, 0, 0) );

				cv::Mat rtc( 3, 4, CV_64F );
				rtc = cameraMatrix * rt;
				double D, Dx, Dy;

				cv::Mat m( 3, 3, CV_64F );
				for( int i = 0; i < 3; i++ ) for( int j = 0; j < 2; j++ )
						m.at<double>(i, j) = rtc.at<double>(i, j);
				for( int i = 0; i < 3; i++ )
						m.at<double>(i, 2) = -screen_coord.at<double>(i, 0);

				D = cv::determinant( m );

				for( int i = 0; i < 3; i++ )
						m.at<double>(i, 0) = -rtc.at<double>(i, 3);
				Dx = cv::determinant( m );

				for( int i = 0; i < 3; i++ )
						m.at<double>(i, 0) = rtc.at<double>(i, 0);
				for( int i = 0; i < 3; i++ )
						m.at<double>(i, 1) = -rtc.at<double>(i, 3);
				Dy = cv::determinant( m );

				printf( "%lf, %lf\n", Dx/D, -Dy/D );
			}
		}
	}


	return 0;
}
