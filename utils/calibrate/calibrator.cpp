#define BOARD_HEIGHT 5
#define BOARD_WIDTH 7
#define SQUARE_WIDTH 2.1
#define SQUARE_HEIGHT 2.2

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include <iostream>
#include <vector>
#include <cstdio>

#include "capture.h"
#include "camera.h"


const int width = 640;
const int height = 480;
uchar* data;


void setWorkingFrame( unsigned char* frame );
void YuvToRgb( unsigned char y, unsigned char u, unsigned char v, unsigned char& r, unsigned char& g, unsigned char& b );
unsigned char clip255( int x );



int main()
{
	IMAGE_CONTEXT *ic;
	XEvent event;

	data = new uchar[ 3*width*height ];
	memset( data, 0, 3*width*height );


	cv::Mat cameraMatrix;
	std::vector <cv::Mat> rvecs, tvecs;
	cv::Mat rot_m( 3, 3, CV_64F );
	cv::Mat rt( 3, 4, CV_64F );


	bool end = false;
	bool paus = false;
	bool matrix_found = false;

	std::vector <std::vector <cv::Point2f> > imagePoints;
	std::vector <cv::Point2f> pointBuf;


	// Init capture
	Camera camera( "/dev/video0", NULL );
	camera.startCamera();


	// Creating a new window
	ic = new_window( "Video", 100, 100, width, height );
	XImage *xImage1 = ic->xImage;
	unsigned char *imageLine1 = (unsigned char*) xImage1 -> data;

	while( !end ) {

		unsigned char* fr;

		// Capturing the frame
		if( !paus ) {
			fr = camera.readFrame();

			if (fr == NULL)
				continue;
		}

		setWorkingFrame( fr );
		cv::Mat frame( height, width, CV_8UC3, data );


		if( matrix_found ) {
			cv::Mat res( 3, 1, CV_64F );
			cv::Mat coord = cv::Mat::ones( 4, 1, CV_64F );

			std::cout << "Enter a coordinate." << std::endl;
			scanf("%lf %lf", &coord.at<double>(0, 0), &coord.at<double>(1, 0));

			res = rt * coord;
			res.at<double>(0, 0) /= res.at<double>(2, 0);
			res.at<double>(1, 0) /= res.at<double>(2, 0);
			res.at<double>(2, 0) = 1.0f;

			cv::circle( frame, cv::Point(res.at<double>(0, 0), res.at<double>(1, 0)), 3, cv::Scalar(0, 0, 255) );
		}


		int pix_c = 0;

		for( int j = 0; j < height; j++ ) {
			for( int i = 0; i < 3*width; i += 3 ) {
				imageLine1[ 4*pix_c + 0 ] = data[j*width*3 + i + 0 ];
				imageLine1[ 4*pix_c + 1 ] = data[j*width*3 + i + 1 ];
				imageLine1[ 4*pix_c + 2 ] = data[j*width*3 + i + 2 ];
				imageLine1[ 4*pix_c + 3 ] = 255;

				pix_c++;
			}
		}

		image_put( ic );
		usleep( 10000 );

		// Event handling
		if ( XPending( ic->display ) > 0 ) {
			XNextEvent( ic->display, &event );

			if( event.type == KeyPress ) {
				if( event.xkey.keycode == 9 ) {		// Esc was pressed
					end = true;
					break;
				}

				if( event.xkey.keycode == 65 ) {	// SPACE was pressed
					paus = !paus;

					if( paus ) {
						pointBuf.clear();

						bool found = cv::findChessboardCorners( frame, cv::Size(BOARD_WIDTH, BOARD_HEIGHT), pointBuf,
                					CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE );

						cv::drawChessboardCorners( frame, cv::Size(BOARD_WIDTH, BOARD_HEIGHT), cv::Mat(pointBuf), found );

						if( found ) imagePoints.push_back( pointBuf );

					}
				}

				if( event.xkey.keycode == 38 ) {	// getting the results 'a' was pressed

					cameraMatrix = cv::Mat::eye( 3, 3, CV_64F );
					std::vector <std::vector <cv::Point3f> > objectPoints( 1 );

					objectPoints[0].clear();
					for( int i = 0; i < BOARD_HEIGHT; i++ )
						for( int j = 0; j < BOARD_WIDTH; j++ )
            	objectPoints[0].push_back(
									cv::Point3f( float( j*SQUARE_WIDTH ), float( i*SQUARE_HEIGHT - (36.0 - SQUARE_HEIGHT) ), 0 )
							);

					objectPoints.resize( imagePoints.size(), objectPoints[0] );

          std::vector<float> distCoeff;
          distCoeff.push_back(0.f);
          distCoeff.push_back(0.f);
          distCoeff.push_back(0.f);
          distCoeff.push_back(0.f);


					double rms = cv::calibrateCamera( objectPoints, imagePoints, cv::Size(width, height), cameraMatrix,
                                 distCoeff, rvecs, tvecs, CV_CALIB_FIX_K4|CV_CALIB_FIX_K5 ); // distCoeffs is not used


					// Output the results
					std::cout << "cameraMatrix: " << cameraMatrix << std::endl;
					std::cout << "rotation: " << rvecs.back() << std::endl;
					std::cout << "translation: " << tvecs.back() << std::endl;

					cv::Rodrigues( rvecs.back(), rot_m );

					for( int i = 0; i < 3; i++ ) for( int j = 0; j < 3; j++ )
						rt.at<double>(i, j) = rot_m.at<double>(i, j);
					for( int i = 0; i < 3; i++ )
						rt.at<double>(i, 3) = tvecs.back().at<double>(i, 0);

					rt = cameraMatrix * rt;

					paus = true;
					matrix_found = true;
				}
			}

			if( event.type == ButtonPress && matrix_found ) {

				cv::Point2f p( event.xbutton.x, event.xbutton.y );
				cv::Mat screen_coord = ( cv::Mat_<double>(3, 1) << event.xbutton.x, event.xbutton.y, 1.0 );
				cv::circle( frame, p, 3, cv::Scalar(255, 0, 0) );

				double D, Dx, Dy;

				cv::Mat m( 3, 3, CV_64F );
				for( int i = 0; i < 3; i++ ) for( int j = 0; j < 2; j++ )
						m.at<double>(i, j) = rt.at<double>(i, j);
				for( int i = 0; i < 3; i++ )
						m.at<double>(i, 2) = -screen_coord.at<double>(i, 0);

				D = cv::determinant( m );

				for( int i = 0; i < 3; i++ )
						m.at<double>(i, 0) = -rt.at<double>(i, 3);
				Dx = cv::determinant( m );

				for( int i = 0; i < 3; i++ )
						m.at<double>(i, 0) = rt.at<double>(i, 0);
				for( int i = 0; i < 3; i++ )
						m.at<double>(i, 1) = -rt.at<double>(i, 3);
				Dy = cv::determinant( m );

				printf( "%lf, %lf\n", Dx/D, Dy/D );
			}
		}
	}

	delete[] data;

	return 0;
}


void setWorkingFrame( unsigned char* frame )
{
	int pix_c = 0;
	unsigned char Y1, Y2, U, V;
	unsigned char r, g, b;

	for( int i = 0; i < 2*width*height; i += 4 ) {

        Y1 = frame[ i + 0 ];
        U = frame[ i + 1 ];
        Y2 = frame[ i + 2 ];
        V = frame[ i + 3 ];

				YuvToRgb( Y1, U, V, r, g, b );

        data [ 3*pix_c + 0 ] = r;
        data [ 3*pix_c + 1 ] = g;
        data [ 3*pix_c + 2 ] = b;

				YuvToRgb( Y2, U, V, r, g, b );

        data [ 3*(pix_c+1) + 0 ] = r;
        data [ 3*(pix_c+1) + 1 ] = g;
        data [ 3*(pix_c+1) + 2 ] = b;

        pix_c += 2;
    }
}


void YuvToRgb( unsigned char y, unsigned char u, unsigned char v, unsigned char& r, unsigned char& g, unsigned char& b )
{
	int C = y - 16;
	int D = u - 128;
	int E = v - 128;

	r = clip255( (298 * C           + 409 * E + 128) >> 8 );
	g = clip255( (298 * C - 100 * D - 208 * E + 128) >> 8 );
	b = clip255( (298 * C + 516 * D           + 128) >> 8 );
}


unsigned char clip255( int x )
{
	if( x > 255 )
		return 255;
	if( x < 0 )
		return 0;
	else
		return x;
}





