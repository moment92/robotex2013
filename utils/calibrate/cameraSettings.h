#ifndef CAMERA_SETTINGS_H_INCLUDED
#define CAMERA_SETTINGS_H_INCLUDED

#include <vector>
#include <string>

class Camera;


struct CameraConfig {
  struct ControlPair {
    std::string name;
    int value;
  };
  
  int fps;
  int width;
  int height;
  std::string pixelFormat;
  std::vector<ControlPair> controls;
  
  bool cameraConfigExists;
};


struct CameraSettings
{
  struct CameraControl
  {
    int id;
    int type;
    int maxValue;
    int minValue;
    int step;
    int value;
    std::string name;
    std::string varName;
    std::vector<std::string> menuItems;
  };
  
  struct SupportedFormat
  {
    struct FormatSize {
      int width;
      int height;
      
      std::vector<int> fps;
    };
    
    std::string name;
    std::vector<FormatSize> formatSizes;
  };
  
  
  int width;
  int height;
  int fps;
  std::string pixelFormat;
  
  std::vector<CameraControl> cameraControls;
  std::vector<SupportedFormat> supportedFormats;
  
  Camera* camera;
};




#endif