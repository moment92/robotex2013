#include "udpConnection.h"

#include <boost/bind.hpp>


using namespace boost;
using namespace boost::asio;



UdpConnection::UdpConnection( std::string host, int port )
  : host( host ),
    port( port ),
    ioRunning( false ),
    receiveBuffer2( buffer(receiveBuffer, udpBufferSize) ),
    udpThread( &UdpConnection::ioServer, this )
{
}



UdpConnection::~UdpConnection()
{
  udpThread.join();
  
  if (socket != NULL)
    delete socket;
}



void UdpConnection::ioServer()
{
  io_service ioService;
  
  socket = new udp::socket( ioService, udp::endpoint(udp::v4(), port) );
  remoteEndpoint = udp::endpoint( ip::address::from_string(host), port );

  receiveNext();
  
  ioRunning = true;
  
  ioService.run();
}


void UdpConnection::receiveNext()
{
  try {
    socket->async_receive_from(
      receiveBuffer2,
      endpoint,
      bind( &UdpConnection::onReceive, this, placeholders::error, placeholders::bytes_transferred )
    );
  } catch (std::exception& e) {
    std::cout << "- Communication receive error: " << e.what() << std::endl;
  }
}



void UdpConnection::onReceive( const boost::system::error_code& error, size_t bytesReceived )
{
  // Implement when needed
  if (ioRunning)
    receiveNext();
}



void UdpConnection::send( std::string message )
{
  char sendBuffer[udpBufferSize];
  message += "\n";

  memcpy( sendBuffer, message.c_str(), message.size() );
  sendBuffer[message.size()] = 0;
  
  try {
    socket->send_to( buffer(sendBuffer, message.length()), remoteEndpoint );
  } catch (std::exception& e) {
    std::cout << "- Communication send error: " << e.what() << std::endl;
  }
}


void UdpConnection::close()
{
  ioRunning = false;
  
  if (socket != NULL) {
    try {
      boost::system::error_code ec;
      socket->shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
      socket->close();
    } catch (std::exception& e) {
      std::cout << "- Communication close error: " << e.what() << std::endl;
    }
  }
}



void UdpConnection::sendSpeeds( int flSpeed, int frSpeed, int rlSpeed, int rrSpeed, int dribblerSpeed )
{
  char speedStr[100];
  memset( speedStr, '\0', 100);
  sprintf( speedStr, "speeds:%d:%d:%d:%d:%d", flSpeed, frSpeed, rlSpeed, rrSpeed, dribblerSpeed );

  send( speedStr );
}

