#include <cstdio>
#include <cmath>
#include "udpConnection.h"

#define PI 3.14159265

using namespace std;


int speed;
float angle;
int wheelFL, wheelFR, wheelRL, wheelRR;



void drive( float angle, int speed )
{
  float xDir, yDir;
  
  xDir = cos(angle) * speed;
  yDir = sin(angle) * speed;
  
  wheelFR = -sin(33.0*PI/180.0) * xDir + cos(33.0*PI/180.0) * yDir;
  wheelFL = -sin(147.0*PI/180.0) * xDir + cos(147.0*PI/180.0) * yDir;
  wheelRL = -sin(225.0*PI/180.0) * xDir + cos(225.0*PI/180.0) * yDir;
  wheelRR = -sin(315.0*PI/180.0) * xDir + cos(315.0*PI/180.0) * yDir;
}



int main()
{
	UdpConnection udpConnection( "192.168.4.1", 8042 );
	
	printf("Enter speed and angle\n");
	
	while(1) {
		scanf("%d", &speed);
		scanf("%f", &angle);

		udpConnection.sendSpeeds( wheelFL, wheelFR, wheelRL, wheelRR, 0 );
	}

	

	return 0;
}
