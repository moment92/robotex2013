#ifndef IMAGE_PROCESSOR_H_INCLUDED
#define Image_PROCESSOR_H_INCLUDED


typedef unsigned char uchar;


class Threshold;
class Segmentation;
class Image;
class FrameInfoHandler;


class ImageProcessor
{
public:
  ImageProcessor( int width, int height );
  ~ImageProcessor( void );
  
  void start( void );
  FrameInfoHandler* process( Image* image );
  Image* getLastThresImage( void );
  
  
private:
  Threshold* threshold;
  Segmentation* segm;
  FrameInfoHandler* frameInfoHandler;
  
  int width;
  int height;
};


#endif