#include "robot.h"
#include "imgProc/displayWindow.h"
#include "imgProc/color.h"
#include <thread>
#include <functional>


volatile bool guiRunning = false;
Robot* pRobot = NULL;

void displayVideo( Robot& robot );
void onKeyPress( int key );



int main(int argc, char** argv)
{
  std::thread* guiThread = NULL;
  bool debug = false;
  bool motors = true;
  Colors gateColor = NoColor;
  
  Robot robot;
  pRobot = &robot;
  
  
  for (int i = 1; i < argc; i++) {
    if (strcmp(argv[i], "--gui") == 0) {
      guiRunning = true;
      guiThread = new std::thread( displayVideo, std::ref(robot) );
    }
    if (strcmp(argv[i], "--debug") == 0)
      debug = true;
    if (strcmp(argv[i], "--no-motors") == 0)
      motors = false;
    if (strcmp(argv[i], "--yellow") == 0)
      gateColor = Yellow;
    if (strcmp(argv[i], "--blue") == 0)
      gateColor = Blue;
  }
  
  
  while (gateColor == NoColor) {
    char chr;
    printf( "Choose the goal --- 'y' or 'b'\n" );
    scanf( "%c", &chr );
    
    if (chr == 'b') gateColor = Blue;
    if (chr == 'y') gateColor = Yellow;
  }
  
  
  robot.run( gateColor, motors, debug );
  
  
  if (guiRunning) {
    guiRunning = false;
    guiThread->join();
    delete guiThread;
  }
  
  return 0;
}



void displayVideo( Robot& robot )
{
  DisplayWindow wnd( "Video", imgWidth, imgHeight, 20, 100 );
  DisplayWindow wndTh( "Thresholded video", imgWidth, imgHeight, 670, 100 );
  wnd.setKeyPressHandler( std::ptr_fun(onKeyPress) );
  wndTh.setKeyPressHandler( std::ptr_fun(onKeyPress) );
  
  while (guiRunning) {
    if (robot.getLastImage() != NULL) {
      wnd.setImage( robot.getLastImage() );
      wndTh.setImage( robot.getLastThresImage() );
      
      wnd.run();
      wndTh.run();
    }
  }
  
  wnd.destroyWindow();
  wndTh.destroyWindow();
}


void onKeyPress( int key )
{
  if (key == 9)		// Esc was pressed
    pRobot->stop();
}






