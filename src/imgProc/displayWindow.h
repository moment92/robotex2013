#ifndef DISPLAY_WINDOW_H_INCLUDED
#define DISPLAY_WINDOW_H_INCLUDED


#include <X11/Xlib.h>
#include <X11/extensions/XShm.h>

#include <mutex>

#include <sigc++/functors/slot.h>

#include "image.h"



typedef unsigned char uchar;


class DisplayWindow
{
  
public:
  DisplayWindow( const char* wndName, int width, int height, int xPos, int yPos );
  ~DisplayWindow( void );
  
  void run( void );
  void destroyWindow( void );
  void resizeWindow( int width_, int height_ );
  
  void setImage( Image* img_ );
  
  void lockDisplay( void );
  void unLockDisplay( void );
  bool isDisplayLocked( void );
  
  void setKeyPressHandler( sigc::slot<void, int> handler );
  void setButtonPressHandler( sigc::slot<void, int, int> handler );
  
  void getPosition( int& x, int& y );
  void setPosition( int x, int y );
  
  
private:
  int imageCreate( void );
  bool showImage( void );
  void drawImage( void );
  
  
public:
  int width;
  int height;
  bool displayReady;
  
  Image* img;
  
  
private:
  Display* display;
  Window window;
  XImage* xImage;
  uchar* displayData;
  GC graphicsContext;
  XShmSegmentInfo shmInfo;
  Atom wmDeleteMessage;
  Screen* screen;
  
  std::mutex displayMutex;
  
  sigc::slot<void, int> onKeyPress;
  sigc::slot<void, int, int> onButtonPress;
};


#endif