#ifndef THRESHOLD_H_INCLUDED
#define THRESHOLD_H_INCLUDED

#define THRES_DATA_SIZE 128*128*128

#include <string>

typedef unsigned char uchar;


class Threshold
{
public:
  Threshold( void );
  ~Threshold( void );
  
  int loadThreshold( std::string fileName );
  int saveThreshold( std::string fileName );
  
  
  inline uchar thresholdColor( uchar col1, uchar col2, uchar col3 ) {
    return *(thresData + ((col1 >> 1 << 14) + (col2 >> 1 << 7) + (col3 >> 1)));
  }
  
  inline bool isThresholdColor( uchar col1, uchar col2, uchar col3, uchar thresholdByColor ) {
    return thresholdColor( col1, col2, col3 ) == thresholdByColor;
  }
  
  inline void assignColor( int col1, int col2, int col3, uchar color ){
    *(thresData + ((col1 >> 1 << 14) + (col2 >> 1 << 7) + (col3 >> 1))) = color;
  }
  
  void assignColorRange( int col1, int col2, int col3, uchar color );
  void removeColorRange( int col1, int col2, int col3, uchar color );
  void resetThreshold( uchar color );

  void setAddColorBrush( int size );
  void setRemoveColorBrush( int size );
  
  void makeBackUp( void );
  void resetToBackUp( void );
  
  
private:
  uchar thresData[THRES_DATA_SIZE];
  uchar thresDataBackUp[THRES_DATA_SIZE];
  
  int addColorBrush;
  int removeColorBrush;
};


#endif