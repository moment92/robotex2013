#ifndef THRES_IMAGE_H_INCLUDED
#define THRES_IMAGE_H_INCLUDED


#include "image.h"
#include "color.h"


class ThresImage : public Image
{
public:
  ThresImage( int width, int height )
    : Image( width, height )
  {}
  
  
  inline void getColor( int xCoord, int yCoord, uchar& col1, uchar& col2, uchar& col3 ) {
    col1 = col2 = col3 = frame[ width*yCoord + xCoord ];
  }
  
  
  inline void getRGB( int xCoord, int yCoord, uchar& r, uchar& g, uchar& b ) {
    uchar col;
    getColor( xCoord, yCoord, col, col, col );
    
    switch (col)
    {
      case Orange:
	r = 238;
	g = 113;
	b = 25;
	break;
      case Green:
	r = 19;
	g = 166;
	b = 50;
	break;
      case Blue:
	r = 46;
	g = 20;
	b = 141;
	break;
      case Yellow:
	r = 255;
	g = 228;
	b = 15;
	break;
      case White:
	r = g = b = 255;
	break;
      case Black:
	r = g = b = 0;
	break;
      case NoColor:
	r = g = b = 128;
	break;
    }
  }
};


#endif