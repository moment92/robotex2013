#ifndef FRAME_INFO_HANDLER_H_INCLUDED
#define FRAME_INFO_HANDLER_H_INCLUDED


#include "segmentation.h"
#include "color.h"




struct Blob
{
  int x1, y1, x2, y2;
  int area;
  Colors color;
  
  Blob( int x1, int y1, int x2, int y2, int area, Colors color )
    : x1( x1 ),
      y1( y1 ),
      x2( x2 ),
      y2( y2 ),
      area( area ),
      color( color )
  {}
  
  int centerX() {
    return x1 + (x2 - x1)/2;
  }
};



class FrameInfoHandler
{
public:
  FrameInfoHandler( void );
  ~FrameInfoHandler( void );
  
  void setFrameInfo( FrameInfo* info );
  Blob getLargestBlob( Colors color, int nthLargest = 1 );
  Colors getColor( int x, int y );
  
  
public:
  unsigned char* thData;
  
  
private:
  FrameInfo* info;
  Blob defaultBlob;
};



#endif