#include "coordinateTranslator.h"

#include <cstdio>
#include <opencv2/calib3d/calib3d.hpp>




CoordinateTranslator::CoordinateTranslator()
  : rt( 3, 4, CV_64F ),
    cameraMatrix( 3, 3, CV_64F ),
    rvec( 3, 1, CV_64F ),
    rtc( 3, 4, CV_64F )
{
}



CoordinateTranslator::~CoordinateTranslator()
{
}



void CoordinateTranslator::loadConf( const char* fileName )
{
  cv::Mat rot_m( 3, 3, CV_64F );

  FILE *fp = fopen( fileName, "r" );
  if( fp == NULL ) {
    printf( "Error. CoordinateTranslator'i confifaili ei leitud.\n" );
    exit( 0 );
  }

  for( int i = 0; i < 3; i++ )
    fscanf( fp, "%lf %lf %lf\n", &cameraMatrix.at<double>(i, 0), &cameraMatrix.at<double>(i, 1), &cameraMatrix.at<double>(i, 2) );
  fscanf( fp, "%lf %lf %lf\n", &rvec.at<double>(0, 0), &rvec.at<double>(1, 0), &rvec.at<double>(2, 0) );
  fscanf( fp, "%lf %lf %lf", &rt.at<double>(0, 3), &rt.at<double>(1, 3), &rt.at<double>(2, 3) );

  cv::Rodrigues( rvec, rot_m );
  for( int i = 0; i < 3; i++ ) for( int j = 0; j < 3; j++ )
    rt.at<double>(i, j) = rot_m.at<double>(i, j);

  fclose( fp );
  
  
  rtc = cameraMatrix * rt;
}



void CoordinateTranslator::translate( int x, int y, int& realX, int& realY )
{
  double D, Dx, Dy;
  cv::Mat screen_coord = ( cv::Mat_<double>(3, 1) << x, y, 1.0 );
  
  
  if (y == 0) {
    realY = 10000;
    realX = 0;
    return;
  }
  
  
  cv::Mat m( 3, 3, CV_64F );
  for( int i = 0; i < 3; i++ ) for( int j = 0; j < 2; j++ )
    m.at<double>(i, j) = rtc.at<double>(i, j);
  for( int i = 0; i < 3; i++ )
    m.at<double>(i, 2) = -screen_coord.at<double>(i, 0);

  D = cv::determinant( m );

  for( int i = 0; i < 3; i++ )
    m.at<double>(i, 0) = -rtc.at<double>(i, 3);
  Dx = cv::determinant( m );

  for( int i = 0; i < 3; i++ )
    m.at<double>(i, 0) = rtc.at<double>(i, 0);
  for( int i = 0; i < 3; i++ )
    m.at<double>(i, 1) = -rtc.at<double>(i, 3);
  Dy = cv::determinant( m );
  
  realX = -Dx / D;
  realY = Dy / D;
}









