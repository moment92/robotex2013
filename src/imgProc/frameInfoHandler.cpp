#include "frameInfoHandler.h"
#include "segmentation.h"

#define NULL __null




FrameInfoHandler::FrameInfoHandler()
  : defaultBlob( -1, -1, -1, -1, -1, NoColor )
{
}



FrameInfoHandler::~FrameInfoHandler()
{
}


void FrameInfoHandler::setFrameInfo( FrameInfo* info )
{
  this->info = info;
}



Blob FrameInfoHandler::getLargestBlob( Colors color, int nthLargest )
{  
  if (color >= ColorCount)
    return defaultBlob;
  
  region* reg = info[color].list;
  if (reg == NULL)
    return defaultBlob;
  
  
  for (int i = 1; i < nthLargest; i++) {
    if (reg->next == NULL)
      return defaultBlob;
    
    reg = reg->next;
  }
  
  return Blob( reg->x1, reg->y1, reg->x2, reg->y2, reg->area, color );
}



Colors FrameInfoHandler::getColor(int x, int y)
{
  return (Colors) thData[ y*640 + x ];
}










