#ifndef SEGMENTATION_H_INCLUDED
#define SEGMENTATION_H_INCLUDED


class Image;
class Threshold;


struct run {
  short x, y, width;
  unsigned char color;
  int parent, next;
};

struct region {
  int color;
  int x1, y1, x2, y2;
  float cen_x, cen_y;
  int area;
  int run_start;
  int iterator_id;
  region *next;
};

struct FrameInfo {
  region *list;
  int num;
  int min_area;
  unsigned char color;
  char *name;
};



class Segmentation
{
public:
  Segmentation( int width, int height );
  ~Segmentation( void );
  
  FrameInfo* processFrame( Image* image, Threshold* thres );
  
  
private:
  void thresholdImage( Image* image, Threshold* thres );
  void EncodeRuns( void );
  void ConnectComponents( void );
  void ExtractRegions( void );
  void SeparateRegions( void );
  void SortRegions( void );
  region *SortRegionListByArea( region *list, int passes );
  
  
public:
  unsigned char* thData;

  
private:
  int width, height;

  run* rle;
  region* regions;
  FrameInfo* frameInfo;
  int run_c;
  int region_c;
  int max_runs;
  int max_reg;
  int max_area;
};


#endif