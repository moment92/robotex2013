#include "segmentation.h"
#include "image.h"
#include "color.h"
#include "threshold.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <algorithm>
#include <exception>


#define CMV_RBITS 6
#define CMV_RADIX (1 << CMV_RBITS)
#define CMV_RMASK (CMV_RADIX-1)



unsigned char thres[3][256];
unsigned char ad_thres[3][256];


Segmentation::Segmentation( int width, int height )
  : width( width ),
    height( height ),
    max_runs( width*height/6 ),
    max_reg( width*height/16 ),
    run_c( 0 ),
    region_c( 0 ),
    max_area( 0 )
{
  thData = new unsigned char[ width*height ];
  rle = new run[ max_runs ];
  regions = new region[ max_reg ];
  frameInfo = new FrameInfo[ ColorCount ];
}


Segmentation::~Segmentation() {
  delete thData;
  if( rle )
    delete rle;
  if( regions )
    delete regions;
  if( frameInfo )
    delete frameInfo;
}



FrameInfo* Segmentation::processFrame( Image* image, Threshold* thres ) {
    thresholdImage( image, thres );
    EncodeRuns();
    ConnectComponents();
    ExtractRegions();
    SeparateRegions();
    SortRegions();

    return frameInfo;
}




void Segmentation::thresholdImage( Image* image, Threshold* thres )
{
  int pix_c = 0;
  unsigned char col1, col2, col3;
  
  
  for (int yCoord = 0; yCoord < height; yCoord++) {
    for (int xCoord = 0; xCoord < width; xCoord++) {
      
      image->getColor( xCoord, yCoord, col1, col2, col3 );
      thData[ pix_c ] = thres->thresholdColor( col1, col2, col3 );
      
      pix_c++;
    }
  }
}


void Segmentation::EncodeRuns()
// Changes the flat array version of the thresholded image into a run
// length encoded version, which speeds up later processing since we
// only have to look at the points where values change.
{
  unsigned char m, save;
  unsigned char* row;
  int x, y, j, l;
  run r;

  r.next = 0;

  // initialize terminator restore
  save = thData[0];


  j = 0;
  for(y = 0; y < height; y++){
    row = &thData[y * width];

    // restore previous terminator and store next
    // one in the first pixel on the next row
    row[0] = save;
    save = row[width];
    row[width] = 255;
    r.y = y;

    x = 0;
    while(x < width){
      m = row[x];
      r.x = x;

      l = x;
      while(row[x] == m) x++;

      if( m != NoColor || x >= width ) {
	r.color = m;
	r.width = x - l;
	r.parent = j;
	rle[j++] = r;


	if(j >= max_runs) {
	  row[width] = save;
	  printf( "Runlength buffer exceeded.\n" );
	  run_c = j;
	  return;
        }
      }
    }
  }

  run_c = j;
}


void Segmentation::ConnectComponents()
// Connect components using four-connecteness so that the runs each
// identify the global parent of the connected region they are a part
// of.  It does this by scanning adjacent rows and merging where
// similar colors overlap.  Used to be union by rank w/ path
// compression, but now it just uses path compression as the global
// parent index, a simpler rank bound in practice.
// WARNING: This code is complicated.  I'm pretty sure it's a correct
//   implementation, but minor changes can easily cause big problems.
//   Read the papers on this library and have a good understanding of
//   tree-based union find before you touch it
{
  int l1, l2;
  run r1, r2;
  int i, j, s;
  int num = run_c;
  run* map = rle;


  // l2 starts on first scan line, l1 starts on second
  l2 = 0;
  l1 = 1;
  while(map[l1].y == 0) l1++; // skip first line

  // Do rest in lock step
  r1 = map[l1];
  r2 = map[l2];
  s = l1;
  while(l1 < num){

    if(r1.color==r2.color && r1.color){
      // case 1: r2.x <= r1.x < r2.x + r2.width
      // case 2: r1.x <= r2.x < r1.x + r1.width
      if((r2.x<=r1.x && r1.x<r2.x+r2.width) ||
	(r1.x<=r2.x && r2.x<r1.x+r1.width)){
        if(s != l1){
          // if we didn't have a parent already, just take this one
          map[l1].parent = r1.parent = r2.parent;
          s = l1;
        }else if(r1.parent != r2.parent){
          // otherwise union two parents if they are different

          // find terminal roots of each path up tree
          i = r1.parent;
          while(i != map[i].parent) i = map[i].parent;
          j = r2.parent;
          while(j != map[j].parent) j = map[j].parent;

          // union and compress paths; use smaller of two possible
          // representative indicies to preserve DAG property
          if(i < j){
	    map[j].parent = i;
            map[l1].parent = map[l2].parent = r1.parent = r2.parent = i;
          }else{
            map[i].parent = j;
            map[l1].parent = map[l2].parent = r1.parent = r2.parent = j;
          }
        }
      }
    }

    // Move to next point where values may change
    i = (r2.x + r2.width) - (r1.x + r1.width);
    if(i >= 0) r1 = map[++l1];
    if(i <= 0) r2 = map[++l2];
  }

  // Now we need to compress all parent paths
  for(i=0; i<num; i++){
    j = map[i].parent;
    map[i].parent = map[j].parent;
  }
}


// sum of integers over range [x,x+w)
inline int range_sum( int x, int w )
{
  return ( w*(2*x + w-1) / 2 );
}



void Segmentation::ExtractRegions()
// Takes the list of runs and formats them into a region table,
// gathering the various statistics along the way.  num is the number
// of runs in the rmap array, and the number of unique regions in
// reg[] (bounded by max_reg) is returned.  Implemented as a single
// pass over the array of runs.
{
  int b, i, n, a;
  run r;

  n = 0;


  for(i=0; i<run_c; i++){
    if( rle[i].color != NoColor ){
      r = rle[i];
      if(r.parent == i){
        // Add new region if this run is a root (i.e. self parented)
        rle[i].parent = b = n;  // renumber to point to region id
        regions[b].color = r.color;
        regions[b].area = r.width;
        regions[b].x1 = r.x;
        regions[b].y1 = r.y;
        regions[b].x2 = r.x + r.width;
        regions[b].y2 = r.y;
        regions[b].cen_x = range_sum(r.x,r.width);
        regions[b].cen_y = r.y * r.width;
	regions[b].run_start = i;
	regions[b].iterator_id = i; // temporarily use to store last run
        n++;
        if(n >= max_reg) {
	  printf( "Regions buffer exceeded.\n" );
	  region_c = max_reg;
	  return;
	}
      }else{
        // Otherwise update region stats incrementally
        b = rle[r.parent].parent;
        rle[i].parent = b; // update parent to identify region id
        regions[b].area += r.width;
        regions[b].x2 = std::max(r.x + r.width,regions[b].x2);
        regions[b].x1 = std::min((int)r.x,regions[b].x1);
        regions[b].y2 = r.y; // last set by lowest run
        regions[b].cen_x += range_sum(r.x,r.width);
        regions[b].cen_y += r.y * r.width;
	// set previous run to point to this one as next
	rle[regions[b].iterator_id].next = i;
	regions[b].iterator_id = i;
      }
    }
  }

  // calculate centroids from stored sums
  for(i=0; i<n; i++){
    a = regions[i].area;
    regions[i].cen_x = (float)regions[i].cen_x / a;
    regions[i].cen_y = (float)regions[i].cen_y / a;
    rle[regions[i].iterator_id].next = 0; // -1;
    regions[i].iterator_id = 0;
    regions[i].x2--; // change to inclusive range
  }

  region_c = n;
}


void Segmentation::SeparateRegions()
// Splits the various regions in the region table a separate list for
// each color.  The lists are threaded through the table using the
// region's 'next' field.  Returns the maximal area of the regions,
// which can be used later to speed up sorting.
{
  region* p;
  int i;
  int c;
  int area;


  // clear out the region list head table
  for (i = 0; i < ColorCount; i++) {
    frameInfo[i].list = NULL;
    frameInfo[i].num  = 0;
	frameInfo[i].min_area = 0;
	frameInfo[i].color = i;
  }

  // step over the table, adding successive
  // regions to the front of each list
  max_area = 0;
  for(i=0; i<region_c; i++){
    p = &regions[i];
    c = p->color;
    area = p->area;

    if(area >= frameInfo[c].min_area){
      if(area > max_area) max_area = area;
      frameInfo[c].num++;
      p->next = frameInfo[c].list;
      frameInfo[c].list = p;
    }
  }
}



void Segmentation::SortRegions()
// Sorts entire region table by area, using the above
// function to sort each threaded region list.
{
  int i, p;

  // do minimal number of passes sufficient to touch all set bits
  p = 0;
  while( max_area != 0 ) {
    max_area >>= CMV_RBITS;
    p++;
  }

  // sort each list
  for (i = 0; i < ColorCount; i++){
    frameInfo[i].list = SortRegionListByArea(frameInfo[i].list, p);
  }
}



region* Segmentation::SortRegionListByArea( region* list, int passes )
// Sorts a list of regions by their area field.
// Uses a linked list based radix sort to process the list.
{
  region *tbl[CMV_RADIX], *p, *pn;
  int slot, shift;
  int i, j;

  // Handle trivial cases
  if(!list || !list->next) return(list);

  // Initialize table
  for(j=0; j<CMV_RADIX; j++) tbl[j] = NULL;

  for(i=0; i<passes; i++){
    // split list into buckets
    shift = CMV_RBITS * i;
    p = list;
    while(p){
      pn = p->next;
      slot = ((p->area) >> shift) & CMV_RMASK;
      p->next = tbl[slot];
      tbl[slot] = p;
      p = pn;
    }

    // integrate back into partially ordered list
    list = NULL;
    for(j=0; j<CMV_RADIX; j++){
      p = tbl[j];
      tbl[j] = NULL; // clear out table for next pass
      while(p){
        pn = p->next;
        p->next = list;
        list = p;
        p = pn;
      }
    }
  }

  return list;
}


