#ifndef COORDINATE_TRANSLATOR_H_INCLUDED
#define COORDINATE_TRANSLATOR_H_INCLUDED


#include <opencv2/core/core.hpp>


class CoordinateTranslator
{
public:
  CoordinateTranslator( void );
  ~CoordinateTranslator( void );
  
  void loadConf( const char* fileName );
  void translate( int x, int y, int& realX, int& realY );
  
  
private:
  cv::Mat rt;
  cv::Mat cameraMatrix;
  cv::Mat rvec;
  cv::Mat rtc;
};



#endif