#ifndef IMAGE_H_INCLUDED
#define IMAGE_H_INCLUDED


typedef unsigned char uchar;


class Image
{
public:
  Image( int width, int height )
    : width( width ),
      height( height ),
      displayed( false ),
      frame( 0 )
  {}
  
  virtual ~Image( void ) {}
  
  inline bool isFrameSet() {
    return frame != 0;
  }
  
  inline void setFrame( uchar* frame_ ) {
    frame = frame_;
    displayed = false;
  }
  
  inline void removeFrame( void ) {
    frame = 0;
  }
  
  virtual void getColor( int xCoord, int yCoord, uchar& col1, uchar& col2, uchar& col3 ) = 0;
  virtual void getRGB( int xCoord, int yCoord, uchar& r, uchar& g, uchar& b ) = 0;

  
public:
  uchar* frame;
  int width;
  int height;
  volatile bool displayed;
};


#endif