#ifndef COLOR_H_INCLUDED
#define COLOR_H_INCLUDED


enum Colors
{
  NoColor = 0,
  Orange = 1,
  Green = 2,
  Blue = 3,
  Yellow = 4,
  White = 5,
  Black = 6
};

const int ColorCount = 6;



typedef unsigned char uchar;


class Color
{
public:
  static inline void YuvToRgb( uchar y, uchar u, uchar v, uchar& r, uchar& g, uchar& b ) {
    int C = y - 16;
    int D = u - 128;
    int E = v - 128;

    r = clip255( (298 * C           + 409 * E + 128) >> 8 );
    g = clip255( (298 * C - 100 * D - 208 * E + 128) >> 8 );
    b = clip255( (298 * C + 516 * D           + 128) >> 8 );
  }


private:
  static inline uchar clip255( int x ) {
    if( x > 255 )
      return 255;
    if( x < 0 )
      return 0;
    else
      return x;
  }
  
};


#endif