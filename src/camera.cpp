#include "camera.h"
#include "cameraLowLevel.h"
#include "cameraConfig.h"

#include <cstring>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>



Camera::Camera( std::string devName_, CameraConf* config_ )
  : fd( -1 ),
    bufferCount( 4 ),
    closed( false ),
    deviceOpen( false ),
    initDone( false ),
    capturing( false ),
    config( config_ )
{
  devName = new char[ sizeof(devName_) ];
  strcpy( (char*)devName, devName_.c_str() );
}



Camera::~Camera()
{
  if (!closed)
    closeCamera();
  
  delete[] devName;
}



int Camera::startCamera()
{
  bool res;
  
  res = openDevice();
  if (res != 0) {
    printf( "Camera openDevice failed.\n" );
    return -1;
  }
  
  getCameraOptions();
  
  if (config != NULL) {
    if (config->cameraConfigExists) {
      if (config->pixelFormat != "default")
	setPixelFormat( config->pixelFormat );
      
      setResolution( config->width, config->height );
      setFPS( config->fps );

      for (int i = 0; i < config->controls.size(); i++) {
	for (int j = 0; j < cameraSettings.cameraControls.size(); j++) {
	  if (config->controls[i].name == cameraSettings.cameraControls[j].varName)
	    setCameraControl( cameraSettings.cameraControls[j], config->controls[i].value );
	}
      }
    }
  } else {
    
    // For some reason it is needed to set resolution, fps or pixel format before complete camera initialization.
    // When none of them are changed then it fails memory allocation at memory map. When initializing it with some other
    // program then it works somehow, probably linux saves the previous settings or something like that.
    setFPS(50);
    setResolution( 640, 480 );
  }
  
  
  res = initDevice();
  if (res != 0) {
    printf( "Camera initDevice failed.\n" );
    return -1;
  }
  
  res = startCapture();
  if (res != 0) {
    printf( "Camera startCapture failed.\n" );
    return -1;
  }
  
  return 0;
}



void Camera::closeCamera()
{
  int res;
  
  res = stopCapture();
  if (res != 0) {
    printf( "Camera stopCapture failed.\n" );
  }
  
  res = unInitDevice();
  if (res != 0) {
    printf( "Camera unInitDevice failed.\n" );
  }
  
  res = closeDevice();
  if (res != 0) {
    printf( "Camera closeDevice failed.\n" );
  }
}



int Camera::openDevice()
{
  struct stat st;
  int res;
  
  if( deviceOpen )
    return 0;
  
  res = stat( devName, &st );
  if (res == -1) {
    printf( "Cannot identify '%s': %d, %s\n", devName, errno, strerror(errno) );
    return errno;
  }
  
  if (! S_ISCHR(st.st_mode) ) {
    printf( "%s is not a device.\n", devName );
    return -1;
  }
  
  fd = open( devName, O_RDWR | O_NONBLOCK, 0 );
  if (fd == -1) {
    printf( "Cannot open '%s': %d, %s\n", devName, errno, strerror(errno) );
    return errno;
  }
  
  deviceOpen = true;
  return 0;
}


int Camera::closeDevice()
{
  if( !deviceOpen )
    return 0;
  
  cameraMutex.lock();
  
  int res = close( fd );
  if (res == -1) {
    printf( "Device Close error %d, %s\n", errno, strerror(errno) );
    cameraMutex.unlock();
    return errno;
  }
  
  cameraMutex.unlock();
  deviceOpen = false;
  return 0;
}



int Camera::initDevice()
{
  v4l2_capability cap;
  v4l2_format fmt;
  int res;
  unsigned int min;
  
  if( !deviceOpen ) {
    printf( "Can't init the device. It has not been opened yet.\n" );
    return -1;
  }
  
  if( initDone )
    return 0;

  
  res = CameraLowLevel::ioctlCommand( fd, VIDIOC_QUERYCAP, &cap );
  if (res == -1) {
    if (errno == EINVAL) {
      printf( "%s is not a V4L2 device.\n", devName);
      return 1;
    } else {
      printf( "VIDIOC_QUERYCAP error %d, %s\n", errno, strerror(errno) );
      return -1;
    }
  }
  
  if ( !(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE) ) {
    printf( "%s is not a video capture device.\n", devName);
    return 1;
  }
  
  if ( !(cap.capabilities & V4L2_CAP_STREAMING) ) {
    printf( "%s does not support streaming i/o\n", devName);
    return 1;
  }
  
  memset( &fmt, 0, sizeof(fmt) );
  fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  
  res = CameraLowLevel::ioctlCommand( fd, VIDIOC_G_FMT, &fmt );
  if (res == -1) {
    printf( "VIDIOC_G_FMT error %d, %s\n", errno, strerror(errno) );
    return -1;
  }
  
  // Buggy driver hack
  min = fmt.fmt.pix.width * 2;
  if( fmt.fmt.pix.bytesperline < min )
    fmt.fmt.pix.bytesperline = min;
  min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
  if( fmt.fmt.pix.sizeimage < min )
    fmt.fmt.pix.sizeimage = min;
  
  
  initMemoryMap();
  
  initDone = true;
  return 0;
}



int Camera::unInitDevice()
{
  int res;
  
  if( !initDone )
    return 0;
  
  cameraMutex.lock();
  
  for (int i = 0; i < bufferCount; i++) {
    res = munmap( buffers[i].start, buffers[i].length );
    if (res == -1) {
      printf( "Memory unmap failed. %d, %s\n", errno, strerror(errno) );
      cameraMutex.unlock();
      return -1;
    }
  }
  
  free( buffers );
  
  cameraMutex.unlock();
  initDone = false;
  return 0;
}




int Camera::initMemoryMap()
{
  v4l2_requestbuffers req;
  int res;
  
  
  memset( &req, 0, sizeof(req) );
  req.count = bufferCount;
  req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  req.memory = V4L2_MEMORY_MMAP;
  
  res = CameraLowLevel::ioctlCommand( fd, VIDIOC_REQBUFS, &req );
  if (res == -1) {
    if (errno == EINVAL) {
      printf( "%s does not support memory mapping.\n", devName );
      return 1;
    } else {
      printf( "VIDIOC_REQBUFS error %d, %s\n", errno, strerror(errno) );
      return -1;
    }
  }
  
  buffers = (ImageBuffer*) calloc( req.count, sizeof(*buffers) );
  if (!buffers) {
    printf( "calloc failed. Not enough memory.\n" );
    return -1;
  }
  
  for (unsigned int i = 0; i < req.count; i++) {
    v4l2_buffer buf;
    
    memset( &buf, 0, sizeof(buf) );
    buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory      = V4L2_MEMORY_MMAP;
    buf.index       = i;
    
    CameraLowLevel::ioctlCommand( fd, VIDIOC_QUERYBUF, &buf );
    
    buffers[i].length = buf.length;
    buffers[i].start = mmap( NULL, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, buf.m.offset );
    
    if (buffers[i].start == MAP_FAILED) {
      printf( "MemoryMap failed. %d, %s\n", errno, strerror(errno) );
      return -1;
    }
  }
  
  return 0;
}




int Camera::startCapture()
{
  v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  int res;
  
  if( !initDone ) {
    printf( "Can't start capturing. Device is not initialized.\n" );
    return -1;
  }
  
  if( capturing )
    return 0;
  
  for (unsigned int i = 0; i < bufferCount; i++) {
    v4l2_buffer buf;
    memset( &buf, 0, sizeof(buf) );
    
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.index = i;
    
    res = CameraLowLevel::ioctlCommand( fd, VIDIOC_QBUF, &buf );
    if (res == -1) {
      printf( "VIDIOC_QBUF error %d, %s\n", errno, strerror(errno) );
      return -1;
    }
  }
  
  res = CameraLowLevel::ioctlCommand( fd, VIDIOC_STREAMON, &type );
  if (res == -1) {
    printf( "VIDIOC_STREAMON error %d, %s\n", errno, strerror(errno) );
    return -1;
  }
  
  capturing = true;
  return 0;
}




int Camera::stopCapture()
{
  if( !capturing )
    return 0;
  
  cameraMutex.lock();
  
  v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  int res;

  res = CameraLowLevel::ioctlCommand( fd, VIDIOC_STREAMOFF, &type );
  if (res == -1) {
    printf( "VIDIOC_STREAMOFF error %d, %s\n", errno, strerror(errno) );
    cameraMutex.unlock();
    return -1;
  }
  
  cameraMutex.unlock();
  capturing = false;
  return 0;
}




uchar* Camera::readFrame()
{
  v4l2_buffer buf;
  int bufferIndex;
  int res;
  
  cameraMutex.lock();
  
  if( capturing ) {
    memset( &buf, 0, sizeof(buf) );
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    
    res = CameraLowLevel::ioctlCommand( fd, VIDIOC_DQBUF, &buf );
    if (res == -1) {
      if (errno != EAGAIN) {	// EAGAIN is OK here, as the device is set to nonblocking mode
	printf( "VIDIOC_DQBUF error %d, %s\n", errno, strerror(errno) );
	exit( EXIT_FAILURE );
      }
      
      cameraMutex.unlock();
      return NULL;
    }
    
    bufferIndex = buf.index;
    
    res = CameraLowLevel::ioctlCommand( fd, VIDIOC_QBUF, &buf );
    if (res == -1) {
      printf( "VIDIOC_QBUF error %d, %s\n", errno, strerror(errno) );
      exit( EXIT_FAILURE );
    }
    
    cameraMutex.unlock();
    
    return (uchar*) buffers[bufferIndex].start;
  } else {
    cameraMutex.unlock();
    return NULL;
  }
}




void Camera::getCameraControls()
{
  v4l2_queryctrl queryctrl;
  CameraSettings::CameraControl camCtrl;
  
  memset( &queryctrl, 0, sizeof(queryctrl) );
  queryctrl.id = V4L2_CTRL_FLAG_NEXT_CTRL;
  
  while (CameraLowLevel::ioctlCommand(fd, VIDIOC_QUERYCTRL, &queryctrl) == 0) {
    if (queryctrl.type != V4L2_CTRL_TYPE_CTRL_CLASS && !(queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)) {
      parseCameraControl( queryctrl, camCtrl );
      cameraSettings.cameraControls.push_back( camCtrl );
    }
    queryctrl.id |= V4L2_CTRL_FLAG_NEXT_CTRL;
  }
  
  if (queryctrl.id != V4L2_CTRL_FLAG_NEXT_CTRL)
    return;
  
  for (queryctrl.id = V4L2_CID_USER_BASE; queryctrl.id < V4L2_CID_LASTP1; queryctrl.id++)
  {
    if (CameraLowLevel::ioctlCommand(fd, VIDIOC_QUERYCTRL, &queryctrl) == 0
      && !(queryctrl.flags & V4L2_CTRL_FLAG_DISABLED))
    {
      parseCameraControl( queryctrl, camCtrl );
      cameraSettings.cameraControls.push_back( camCtrl );
    }
  }
  
  for ( queryctrl.id = V4L2_CID_PRIVATE_BASE;
    CameraLowLevel::ioctlCommand(fd, VIDIOC_QUERYCTRL, &queryctrl) == 0;
    queryctrl.id++ )
  {
    if (! (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED) ) {
      parseCameraControl( queryctrl, camCtrl );
      cameraSettings.cameraControls.push_back( camCtrl );
    }
  }
}



void Camera::parseCameraControl( v4l2_queryctrl& v4l2_ctrl, CameraSettings::CameraControl& camCtrl )
{
  v4l2_control control;
  v4l2_querymenu querymenu;
  
  
  camCtrl.id = v4l2_ctrl.id;
  camCtrl.name = (const char*) v4l2_ctrl.name;
  camCtrl.varName = getVarName( (const char*) v4l2_ctrl.name );
  camCtrl.type = v4l2_ctrl.type;
  camCtrl.maxValue = v4l2_ctrl.maximum;
  camCtrl.minValue = v4l2_ctrl.minimum;
  camCtrl.step = v4l2_ctrl.step;
  
  memset( &control, 0, sizeof(control) );
  control.id = v4l2_ctrl.id;
  
  if (CameraLowLevel::ioctlCommand(fd, VIDIOC_G_CTRL, &control) == 0)
    camCtrl.value = control.value;
  else
    camCtrl.value = 0;
  
  if (v4l2_ctrl.type == V4L2_CTRL_TYPE_MENU) {
    memset( &querymenu, 0, sizeof(querymenu) );
    querymenu.id = v4l2_ctrl.id;
    
    for ( querymenu.index = v4l2_ctrl.minimum; 
	  querymenu.index <= v4l2_ctrl.maximum;
	  querymenu.index++ )
    {
      if (CameraLowLevel::ioctlCommand(fd, VIDIOC_QUERYMENU, &querymenu) == 0)
	camCtrl.menuItems.push_back( (const char*) querymenu.name );
    }
  }
}



int Camera::setCameraControl( CameraSettings::CameraControl& ctrl, int value )
{
  v4l2_control control;
  int res;
  
  memset( &control, 0, sizeof(control) );

  control.id = ctrl.id;
  control.value = value;
  res = CameraLowLevel::ioctlCommand( fd, VIDIOC_S_CTRL, &control );
  if (res == 0)
    ctrl.value = value;
  
  return res;
}



void Camera::getCameraOptions()
{
  v4l2_format vfmt;
  v4l2_streamparm parm;
  
  vfmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  
  if (CameraLowLevel::ioctlCommand(fd, VIDIOC_G_FMT, &vfmt) == 0) {
    cameraSettings.width = vfmt.fmt.pix.width;
    cameraSettings.height = vfmt.fmt.pix.height;
    cameraSettings.pixelFormat = fcc2s( vfmt.fmt.pix.pixelformat );
  }
  if (CameraLowLevel::ioctlCommand(fd, VIDIOC_G_PARM, &parm) == 0) {
    const v4l2_fract& tf = parm.parm.capture.timeperframe;
    cameraSettings.fps = tf.denominator / tf.numerator;
  }
  
  getSupportedOptions();
  getCameraControls();
  
  cameraSettings.camera = this;
}



void Camera::getSupportedOptions()
{
  v4l2_fmtdesc fmt;
  v4l2_frmsizeenum frmsize;
  v4l2_frmivalenum frmival;
  
  fmt.index = 0;
  fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  
  while (CameraLowLevel::ioctlCommand(fd, VIDIOC_ENUM_FMT, &fmt) >= 0) {
    CameraSettings::SupportedFormat sFormat;
    sFormat.name = fcc2s( fmt.pixelformat );
    
    frmsize.pixel_format = fmt.pixelformat;
    frmsize.index = 0;
    
    while (CameraLowLevel::ioctlCommand(fd, VIDIOC_ENUM_FRAMESIZES, &frmsize) >= 0) {
      CameraSettings::SupportedFormat::FormatSize formatSize;
      
      if (frmsize.type == V4L2_FRMSIZE_TYPE_DISCRETE) {
	formatSize.width = frmsize.discrete.width;
        formatSize.height = frmsize.discrete.height;
	
	frmival.pixel_format = fmt.pixelformat;
        frmival.width = frmsize.discrete.width;
        frmival.height = frmsize.discrete.height;
        frmival.index = 0;
	
	while (CameraLowLevel::ioctlCommand(fd, VIDIOC_ENUM_FRAMEINTERVALS, &frmival) >= 0) {
	  formatSize.fps.push_back( frmival.discrete.denominator / frmival.discrete.numerator );
	  frmival.index++;
	}
	
	sFormat.formatSizes.push_back( formatSize );
      } else {
	printf( "Error. Framesize type is not V4L2_FRMSIZE_TYPE_DISCRETE (unimplemented format)\n" );
      }
      frmsize.index++;
    }
    
    cameraSettings.supportedFormats.push_back( sFormat );
    fmt.index++;
  }
}



int Camera::setFPS( int fps )
{
  bool captureState = capturing;
  
  v4l2_streamparm parm;
  memset( &parm, 0, sizeof(parm) );
  
  parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  parm.parm.capture.timeperframe.numerator = 1000;
  parm.parm.capture.timeperframe.denominator = fps * 1000;
  
  // Can't change FPS while capturing, If you try then it gives errno 16: Device or resource busy
  if (captureState) stopCapture();
  int res = CameraLowLevel::ioctlCommand( fd, VIDIOC_S_PARM, &parm );
  if (captureState) startCapture();
  
  if (res == 0)
    cameraSettings.fps = fps;
  
  return res;
}



int Camera::setResolution( int width, int height )
{
  bool captureState = capturing;
  bool initState = initDone;
  
  v4l2_format in_vfmt;
  int res = -1;
  
  in_vfmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  
  if (CameraLowLevel::ioctlCommand(fd, VIDIOC_G_FMT, &in_vfmt) == 0) {
    in_vfmt.fmt.pix.width = width;
    in_vfmt.fmt.pix.height = height;

    if (captureState) stopCapture();
    if (initState) unInitDevice();
    closeDevice();
    openDevice();
    
    res = CameraLowLevel::ioctlCommand( fd, VIDIOC_S_FMT, &in_vfmt );

    if (initState) initDevice();
    if (captureState) startCapture();
  }
  
  
  if (res == 0) {
    cameraSettings.width = width;
    cameraSettings.height = height;
  }
  
  return res;
}



int Camera::setPixelFormat( std::string name )
{
  bool captureState = capturing;
  bool initState = initDone;
  
  v4l2_format in_vfmt;
  int res = -1;
  int fmtId = -1;
  
  // Finding the format Id
  for (int i = 0; i < cameraSettings.supportedFormats.size(); i++) {
    if (cameraSettings.supportedFormats[i].name == name) {
      fmtId = i;
      break;
    }
  }
  if (fmtId == -1) {
    printf( "Requested pixel format was not found in the supported formats list.\n" );
    return -1;
  }
  
  in_vfmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  
  if (CameraLowLevel::ioctlCommand(fd, VIDIOC_G_FMT, &in_vfmt) == 0) {
    int realId = findPixelFormat( fmtId );
    if( realId == -1 ) {
      printf( "Failed to detect the correct format ID.\n" );
      return -1;
    }
    in_vfmt.fmt.pix.pixelformat = realId; 
    
    if (captureState) stopCapture();
    if (initState) unInitDevice();
    closeDevice();
    openDevice();
    
    res = CameraLowLevel::ioctlCommand( fd, VIDIOC_S_FMT, &in_vfmt );

    if (initState) initDevice();
    if (captureState) startCapture();
  }
  
  if (res == 0)
    cameraSettings.pixelFormat = name;
  
  return res;
}



int Camera::findPixelFormat( unsigned int index )
{
  struct v4l2_fmtdesc fmt;

  fmt.index = index;
  fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  
  if (CameraLowLevel::ioctlCommand( fd, VIDIOC_ENUM_FMT, &fmt))
    return -1;
  
  return fmt.pixelformat;
}



std::string Camera::fcc2s( unsigned int val )
{
  std::string s;

  s += val & 0xff;
  s += (val >> 8) & 0xff;
  s += (val >> 16) & 0xff;
  s += (val >> 24) & 0xff;
  
  return s;
}


std::string Camera::getVarName( std::string name )
{
  std::string s;
  int add_underscore = 0;

  for (int i = 0; i < name.length(); i++) {
    if (isalnum(name[i])) {
      if (add_underscore)
	s += '_';
      add_underscore = 0;
      s += std::string(1, tolower(name[i]));
    }
    else if (s.length()) add_underscore = 1;
  }
  return s;
}




