#ifndef UDP_CONNECTION_H_INCLUDED
#define UDP_CONNECTION_H_INCLUDED


#include <string>
#include <thread>
#include <boost/asio.hpp>


using boost::asio::ip::udp;


const int udpBufferSize = 10240;


class UdpConnection
{
public:
  UdpConnection( std::string host, int port );
  ~UdpConnection( void );
  
  void sendSpeeds( int flSpeed, int frSpeed, int rlSpeed, int rrSpeed, int dribblerSpeed );
  void sendKick( int power );
  void sendCharge( void );
  void close( void );
  void send( std::string message );
  
  
  
public:
  bool gotBall;
  
  
private:
  void ioServer( void );
  void receiveNext( void );
  void onReceive( const boost::system::error_code& error, size_t bytesReceived );
  
  
private:
  std::string host;
  int port;
  
  std::thread udpThread;
  bool ioRunning;

  udp::socket* socket;
  udp::endpoint remoteEndpoint;
  udp::endpoint endpoint;
  
  char receiveBuffer[udpBufferSize];
  boost::asio::mutable_buffers_1 receiveBuffer2;
};


#endif