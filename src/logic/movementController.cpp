#include "movementController.h"
#include "../wheel.h"
#include <cmath>

#define PI 3.14159265




MovementController::MovementController( Wheel* wheelFL, Wheel* wheelFR, Wheel* wheelRL, Wheel* wheelRR )
  : wheelFL( wheelFL ),
    wheelFR( wheelFR ),
    wheelRL( wheelRL ),
    wheelRR( wheelRR )
{
}



MovementController::~MovementController()
{
}



void MovementController::drive( float angle, int forwardSpeed, int rotateSpeed )
{
  drive( angle, forwardSpeed );
  addRotation( rotateSpeed );
}



void MovementController::drive( float angle, int speed )
{
  float xDir, yDir;
  
  xDir = cos(angle + PI/2) * speed;
  yDir = sin(angle + PI/2) * speed;
  
  wheelFR->speed = -sin(33.0*PI/180.0) * xDir + cos(33.0*PI/180.0) * yDir;
  wheelFL->speed = -sin(147.0*PI/180.0) * xDir + cos(147.0*PI/180.0) * yDir;
  wheelRL->speed = -sin(225.0*PI/180.0) * xDir + cos(225.0*PI/180.0) * yDir;
  wheelRR->speed = -sin(315.0*PI/180.0) * xDir + cos(315.0*PI/180.0) * yDir;
}



void MovementController::rotate( float speed )
{
  wheelFL->speed = speed;
  wheelFR->speed = speed;
  wheelRL->speed = speed;
  wheelRR->speed = speed;
}


void MovementController::addRotation( int speed )
{
  wheelFL->speed += speed;
  wheelFR->speed += speed;
  wheelRL->speed += speed;
  wheelRR->speed += speed;
}



void MovementController::addMovement( float angle, int speed )
{
  float xDir, yDir;
  
  xDir = cos(angle + PI/2) * speed;
  yDir = sin(angle + PI/2) * speed;
  
  wheelFR->speed += -sin(33.0*PI/180.0) * xDir + cos(33.0*PI/180.0) * yDir;
  wheelFL->speed += -sin(147.0*PI/180.0) * xDir + cos(147.0*PI/180.0) * yDir;
  wheelRL->speed += -sin(225.0*PI/180.0) * xDir + cos(225.0*PI/180.0) * yDir;
  wheelRR->speed += -sin(315.0*PI/180.0) * xDir + cos(315.0*PI/180.0) * yDir;
}









