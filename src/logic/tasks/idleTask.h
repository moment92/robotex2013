#ifndef IDLE_TASK_H_INCLUDED
#define IDLE_TASK_H_INCLUDED


#include "task.h"


enum IdleState
{
  BlueIdle,
  YellowIdle
};


class IdleTask : public Task
{
public:
  IdleTask( void );
  
  TaskResponse* process( FrameInfoHandler*, MoveLogic* );
  bool endCondition( void );
  
  
private:
  TaskResponse* createResponse( FrameInfoHandler* );
  
  
  
private:
  IdleState idleState;
};



#endif