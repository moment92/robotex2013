#ifndef ATTACK_BALL_TASK_H_INCLUDED
#define ATTACK_BALL_TASK_H_INCLUDED


#include "task.h"


class AttackBallTask : public Task
{
public:
  AttackBallTask( void );
  
  TaskResponse* process( FrameInfoHandler*, MoveLogic* );
  bool endCondition( void );
  
  
private:
  int counter;
  bool startCounter;
  double lastAngle;
};



#endif