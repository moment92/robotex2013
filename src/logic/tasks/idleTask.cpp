#include "idleTask.h"
#include "../../imgProc/color.h"
#include "../../imgProc/frameInfoHandler.h"
#include "../moveLogic.h"
#include "../movementController.h"


const int gateMinArea = 100;
const int gateNearArea = 15000;
const int gateSeachRotate = 3;


IdleTask::IdleTask()
  : Task( Idle, 0 ),
    idleState( BlueIdle )
{
  printf("IdleTask create\n");
}



TaskResponse* IdleTask::process( FrameInfoHandler* frameInfoHandler, MoveLogic* moveLogic )
{
  Colors gateColor;
  
  idleState == BlueIdle ? gateColor=Blue : gateColor=Yellow;
  Blob blob = frameInfoHandler->getLargestBlob( gateColor );
  
  
  if (blob.area > gateMinArea) {
    moveLogic->driveToScreenCoord( blob.centerX(), blob.y1 );

    if (blob.area > gateNearArea)
      idleState == BlueIdle ? idleState=YellowIdle : idleState=BlueIdle;
  } else
    moveLogic->moveCtrl->rotate( gateSeachRotate );
  
  
  return createResponse( frameInfoHandler );
}




bool IdleTask::endCondition()
{
  return false;
}



TaskResponse* IdleTask::createResponse( FrameInfoHandler* frameInfoHandler )
{
  Blob blob = frameInfoHandler->getLargestBlob( Orange );
  
  if (blob.area > 0)
    response.ballSeen = true;
  else
    response.ballSeen = false;
  
  return &response;
}












