#ifndef TASK_RESPONSE_H_INCLUDED
#define TASK_RESPONSE_H_INCLUDED


#include <string>



enum TaskType
{
  Idle,
  TrackBall,
  TrackGate,
  Kick,
  Generic,
  DriveCircle,
  Stop,
  AttackBall
};



class TaskResponse
{
public:
  TaskResponse( TaskType type )
    : taskType( type ),
      ballSeen( false ),
      gotBall( false ),
      goalAlligned( false )
  {}
  
  
public:
  TaskType taskType;
  
  bool ballSeen;
  bool gotBall;
  bool goalAlligned;
  
  int ballDistance;
  double goalAngle;
  double ballAngle;
};



#endif