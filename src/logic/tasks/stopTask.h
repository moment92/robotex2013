#ifndef STOP_TASK_H_INCLUDED
#define STOP_TASK_H_INCLUDED


#include "task.h"


class StopTask : public Task
{
public:
  StopTask( void );
  
  TaskResponse* process( FrameInfoHandler*, MoveLogic* );
  bool endCondition( void );
};



#endif