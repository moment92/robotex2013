#include "trackBallTask.h"

#include <cstdio>
#include "../../imgProc/coordinateTranslator.h"
#include "../moveLogic.h"
#include "../movementController.h"



TrackBallTask::TrackBallTask()
  : Task( TrackBall, 3 ),
    end( false )
{
  printf("TrackBallTask create\n");
}



TaskResponse* TrackBallTask::process( FrameInfoHandler* frameInfoHandler, MoveLogic* moveLogic )
{
  int counter = 1;
  int realX, realY;
  Blob blob(-1, -1, -1, -1, -1, NoColor);
  
  
  do {
    blob = frameInfoHandler->getLargestBlob( Orange, counter );
    
    if (checkBall(frameInfoHandler, &blob))
      break;
    counter++;
  } while(blob.area > 0);
  
  if (blob.area != -1) {
    printf( "I can see the ball! (%d %d), (%d %d)\n", blob.x1, blob.y1, blob.x2, blob.y2 );
    moveLogic->coordTransl->translate( blob.centerX(), blob.y1, realX, realY );
    response.ballDistance = sqrt( realX*realX + realY*realY );
    
    if (response.ballDistance > 30)
      moveLogic->driveToScreenCoord( blob.centerX(), blob.y1 );
    else
      moveLogic->moveCtrl->drive( 0, 0 );
    
    response.ballSeen = true;
    moveLogic->coordTransl->translate( blob.centerX(), blob.y1, realX, realY );
    response.ballDistance = sqrt( realX*realX + realY*realY );
  } else
    response.ballSeen = false;
  
  return &response;
}



bool TrackBallTask::endCondition()
{
  return !response.ballSeen;
}



bool TrackBallTask::checkBall( FrameInfoHandler* frameInfoHandler, Blob* blob )
{
  int remX=0, addX=0;
  int greenC = 0;
  if (blob->x1 > 30) remX=20;
  if (blob->x2 < 610) addX=20;
  
  if (frameInfoHandler->getColor(blob->x1-remX, blob->y1) == Green) greenC++;
  if (frameInfoHandler->getColor(blob->x1-remX, blob->y2) == Green) greenC++;
  if (frameInfoHandler->getColor(blob->x2+addX, blob->y1) == Green) greenC++;
  if (frameInfoHandler->getColor(blob->x2+addX, blob->y2) == Green) greenC++;
  
  return greenC > 1;
}






