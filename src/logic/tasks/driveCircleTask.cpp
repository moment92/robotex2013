#include "driveCircleTask.h"

#include <cstdio>
#include <cmath>
#include "../../imgProc/frameInfoHandler.h"
#include "../../imgProc/coordinateTranslator.h"
#include "../moveLogic.h"
#include "../movementController.h"



driveCircleTask::driveCircleTask()
  : Task( DriveCircle, 4),
    end( false )
{
  printf("DriveCircleTask create\n");
}



TaskResponse* driveCircleTask::process( FrameInfoHandler* frameInfoHandler, MoveLogic* moveLogic )
{
  int rBallX, rBallY, rGateX, rGateY;
  Blob blobBall = frameInfoHandler->getLargestBlob( Orange ); 
  Blob blobGate = frameInfoHandler->getLargestBlob( moveLogic->gateColor );
  
  blobBall.area > 0 ? response.ballSeen=true : response.ballSeen=false;
  
  if (blobBall.area > 0) {
    if (blobGate.area > 100) {
      moveLogic->driveInCircleWithGateScreen( blobBall.centerX(), blobBall.y1, blobGate.centerX(), blobGate.y1 );
      printf("DriveInCircleGate\n");
    } else {
      moveLogic->driveInCircleScreen( blobBall.centerX(), blobBall.y1 );
      printf("DriveInCircle noGate\n");
    }
    
    moveLogic->coordTransl->translate( blobBall.centerX(), blobBall.y1, rBallX, rBallY );
    response.ballAngle = atan( (double) rBallX / (double) rBallY );
  }
  
  if (blobGate.area > 100 && 290 <= blobGate.centerX() && blobGate.centerX() <= 360) {
    response.goalAlligned = true;
    printf("Gate aligned\n");
  }
  else
    response.goalAlligned = false;
  
  
  return &response;
}



bool driveCircleTask::endCondition()
{
  if (response.ballSeen) {
    if (response.ballDistance < 80)
      return response.goalAlligned;
    else
      return true;
  }
  else
    return true;
}

