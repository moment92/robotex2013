#ifndef TASK_H_INCLUDED
#define TASK_H_INCLUDED


#include "taskResponse.h"



class FrameInfoHandler;
class MoveLogic;



class Task
{
public:
  Task( TaskType type, int priority )
    : priority( priority ),
      response( type )
  {}
  
  virtual ~Task( void ) {}
  
  virtual TaskResponse* process( FrameInfoHandler*, MoveLogic* ) { return &response; }
  virtual bool endCondition( void ) { return true; };
  
  
  bool operator< (const Task& task) const
  {
    return this->priority < task.priority;
  }
  
  bool operator> (const Task& task) const
  {
    return this->priority > task.priority;
  }
  
  
public:
  int priority;
  TaskResponse response;
  
};


#endif