#include "stopTask.h"

#include "../../imgProc/color.h"
#include "../../imgProc/frameInfoHandler.h"
#include "../moveLogic.h"
#include "../movementController.h"



StopTask::StopTask()
  : Task( Stop, 99999 )
{
  printf("StopTask create\n");
}



TaskResponse* StopTask::process( FrameInfoHandler* frameInfoHandler, MoveLogic* moveLogic )
{
  printf("StopTask process\n");
  moveLogic->moveCtrl->drive( 0, 0, 0 );
  return &response;
}



bool StopTask::endCondition(void )
{
  return false;
}