#include "attackBallTask.h"
#include "../moveLogic.h"
#include "../movementController.h"
#include "../../imgProc/color.h"
#include "../../imgProc/frameInfoHandler.h"
#include "../../imgProc/coordinateTranslator.h"



AttackBallTask::AttackBallTask()
  : Task( AttackBall, 30 ),
    startCounter( false ),
    counter( 0 ),
    lastAngle( 0.0 )
{
  printf("AttackBallTask create\n");
}



TaskResponse* AttackBallTask::process( FrameInfoHandler* frameInfoHandler, MoveLogic* moveLogic )
{
  double angle;
  Blob blob = frameInfoHandler->getLargestBlob( Orange );
  moveLogic->driveToScreenCoord( blob.centerX(), blob.y1 );
  
  blob.area > 0 ? response.ballSeen=true : response.ballSeen=false;
  return &response;
}



bool AttackBallTask::endCondition()
{
  return !response.ballSeen;
}
