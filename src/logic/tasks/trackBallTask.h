#ifndef TRACK_BALL_TASK_H_INCLUDED
#define TRACK_BALL_TASK_H_INCLUDED


#include "task.h"
#include "../../imgProc/frameInfoHandler.h"


class TrackBallTask : public Task
{
public:
  TrackBallTask( void );
  
  TaskResponse* process( FrameInfoHandler*, MoveLogic* );
  bool endCondition( void );
  
  
private:
  bool checkBall( FrameInfoHandler*, Blob* );
  
  
private:
  bool end;
};



#endif