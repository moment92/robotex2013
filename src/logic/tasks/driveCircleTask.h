#ifndef DRIVE_CIRCLE_TASK_H_INCLUDED
#define DRIVE_CIRCLE_TASK_H_INCLUDED


#include "task.h"


class driveCircleTask : public Task
{
public:
  driveCircleTask( void );
  
  TaskResponse* process( FrameInfoHandler*, MoveLogic* );
  bool endCondition( void );
  
  
private:
  bool end;
};



#endif