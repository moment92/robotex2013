#ifndef MOVE_LOGIC_H_INCLUDED
#define MOVE_LOGIC_H_INCLUDED


#include "../imgProc/color.h"



class MovementController;
class CoordinateTranslator;


const int moveSpeed = 20;
const int correctionRotationSpeed = 1;
const int rotationSpeed = 3;



class MoveLogic
{
public:
  MoveLogic( MovementController* moveCtrl, CoordinateTranslator* coordTransl );
  ~MoveLogic( void );
  
  void setGateColor( Colors gateColor );
  
  void driveToScreenCoord( int screenX, int screenY );
  void driveToRealCoord( int realX, int realY );
  void driveInCircleScreen( int screenX, int screenY );
  void driveInCircleReal( int realX, int realY );
  void driveInCircleWithGateScreen( int scBallX, int scBallY, int scGateX, int scGateY );
  void driveInCircleWithGateReal( int rBallX, int rBallY, int rGateX, int rGateY );
  
  
public:
  MovementController* moveCtrl;
  CoordinateTranslator* coordTransl;
  
  Colors gateColor;
};


#endif
