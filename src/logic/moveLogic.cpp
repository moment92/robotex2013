#include "moveLogic.h"
#include "movementController.h"
#include "../imgProc/coordinateTranslator.h"

#include <cmath>


const int moveSideSpeed = 10;
const int circleRotSpeed = 3;




MoveLogic::MoveLogic( MovementController* moveCtrl, CoordinateTranslator* coordTransl )
  : moveCtrl( moveCtrl ),
    coordTransl( coordTransl )
{
}



MoveLogic::~MoveLogic()
{
}



void MoveLogic::setGateColor( Colors gateColor )
{
  this->gateColor = gateColor;
}



void MoveLogic::driveToScreenCoord( int screenX, int screenY )
{
  int realX, realY;
  
  coordTransl->translate( screenX, screenY, realX, realY );
  driveToRealCoord( realX, realY );
}



void MoveLogic::driveToRealCoord( int realX, int realY )
{
  double angle;
  int rotSpeed = 0;
  
  angle = atan( (double) realX / (double) realY );
  
  if (angle > 0.1)
    rotSpeed = correctionRotationSpeed;
  else if (angle < -0.1)
    rotSpeed = -correctionRotationSpeed;
    
  moveCtrl->drive( angle, moveSpeed, rotSpeed );
}



void MoveLogic::driveInCircleScreen( int screenX, int screenY )
{
  int realX, realY;
  
  coordTransl->translate( screenX, screenY, realX, realY );
  driveInCircleReal( realX, realY );
}



void MoveLogic::driveInCircleReal( int realX, int realY )
{
  int neg = 0;
  realX > 0 ? neg=1 : neg=-1;
  moveCtrl->drive( neg*90.0*3.14/180.0, moveSideSpeed, -circleRotSpeed*neg );
  /*if (realY > 30) moveCtrl->addMovement( 0, 3 );
  if (realY < 20) moveCtrl->addMovement( 0, -3 );*/
}



void MoveLogic::driveInCircleWithGateScreen( int scBallX, int scBallY, int scGateX, int scGateY )
{
  int rBallX, rBallY, rGateX, rGateY;
  coordTransl->translate( scBallX, scBallY, rBallX, rBallY );
  coordTransl->translate( scGateX, scGateY, rGateX, rGateY );
  
  driveInCircleWithGateReal( rBallX, rBallY, rGateX, rGateY );
}



void MoveLogic::driveInCircleWithGateReal( int rBallX, int rBallY, int rGateX, int rGateY )
{
  double angleGate, angleBall;
  int negGate, negBall;
  
  angleGate = atan( (double) rGateX / (double) rGateY );
  angleBall = atan( (double) rBallX / (double) rBallY );
  
  if (angleGate > 0.0) {
    printf("Left\n");
    negGate = -1;
  }
  else if (angleGate < 0.0) {
    printf("Right\n");
    negGate = 1;
  } else
    negGate = 0;
  
  if (angleBall > 0.0) {
    printf("Left\n");
    negBall = 1;
  }
  else if (angleBall <= 0.0) {
    printf("Right\n");
    negBall = -1;
  }
  
  moveCtrl->drive( negGate*90.0*3.14/180.0, moveSideSpeed, negBall*circleRotSpeed );
  /*if (rBallY > 30) moveCtrl->addMovement( 0, 3 );
  if (rBallY < 20) moveCtrl->addMovement( 0, -3 );*/
}









