#ifndef MOVEMENT_CONTROLLER_H_INCLUDED
#define MOVEMENT_CONTROLLER_H_INCLUDED


class Wheel;


class MovementController
{
public:
  MovementController( Wheel* wheelFL, Wheel* wheelFR, Wheel* wheelRL, Wheel* wheelRR );
  ~MovementController( void );
  
  void drive( float angle, int forwardSpeed, int rotateSpeed );
  void drive( float angle, int speed );
  void rotate( float speed );
  void addMovement( float angle, int speed );
  
  
private:
  void addRotation( int speed );
  
  
private:
  Wheel* wheelFL;
  Wheel* wheelFR;
  Wheel* wheelRL;
  Wheel* wheelRR;
};




#endif