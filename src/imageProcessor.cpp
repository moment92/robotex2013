#include "imageProcessor.h"
#include "imgProc/yuyvImage.h"
#include "imgProc/thresImage.h"
#include "imgProc/threshold.h"
#include "imgProc/segmentation.h"
#include "imgProc/frameInfoHandler.h"



ImageProcessor::ImageProcessor( int width, int height )
  : width( width ),
    height( height )
{
  threshold = new Threshold();
  segm = new Segmentation( width, height );
  frameInfoHandler = new FrameInfoHandler();
}



ImageProcessor::~ImageProcessor()
{
  if (threshold != NULL)
    delete threshold;
  
  if (segm != NULL)
    delete segm;
  
  if (frameInfoHandler != NULL)
    delete frameInfoHandler;
}


void ImageProcessor::start()
{
  int res = threshold->loadThreshold( "colorTable" );
  if (res != 0) {
    printf( "Failed to load colorTable.\n" );
    exit( -1 );
  }
}


FrameInfoHandler* ImageProcessor::process( Image* img )
{
  FrameInfo* info = segm->processFrame( img, threshold );
  frameInfoHandler->setFrameInfo( info );

  frameInfoHandler->thData = segm->thData;
  
  return frameInfoHandler;
}


Image* ImageProcessor::getLastThresImage()
{
  Image* img = new ThresImage( width, height );
  img->setFrame( segm->thData );
  
  return img;
}






