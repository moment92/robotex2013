#include "robot.h"
#include "imgProc/yuyvImage.h"
#include "imgProc/frameInfoHandler.h"



Robot::Robot()
  : logic( &dribbler, &coilgun, &wheelFL, &wheelFR, &wheelRL, &wheelRR ),
    udpConnection( "192.168.4.1", 8042 ),
    camera( "/dev/video0", &cameraConfig.cameraConfig ),
    imageProcessor( imgWidth, imgHeight ),
    image( NULL )
{
  int res = camera.startCamera();
  if (res != 0) exit(-1);
  
  image = new YuyvImage( imgWidth, imgHeight );
}



Robot::~Robot()
{
  if (image != NULL)
    delete image;
}



void Robot::run( Colors gateColor, bool motors, bool debug )
{
  FrameInfoHandler* frameInfoHandler;
  
  imageProcessor.start();
  if (debug)
    debugConnection.start();
  
  running = true;
  
  if (motors)
    udpConnection.sendCharge();
  
  logic.setGateColor( gateColor );
  
  
  while (running) {
    uchar* frame = camera.readFrame();
    
    if (frame != NULL) {
      image->setFrame( frame );
      frameInfoHandler = imageProcessor.process( image );
      
      logic.work( frameInfoHandler );
      
      
      if (udpConnection.gotBall)
	coilgun.kick(5000);		// HACK: Idiotic way, but too lazy to do it in logic
      
      if (coilgun.kickNow) {
	udpConnection.sendKick( coilgun.power );
	coilgun.noKick();
      }
      
      if (motors)
	udpConnection.sendSpeeds( wheelFL.speed, wheelFR.speed, wheelRL.speed, wheelRR.speed, dribbler.speed );
      if (debug)
	debugConnection.writeDebugInfo( frameInfoHandler );
    }

  }
  
  
  udpConnection.close();
  if (debug)
    debugConnection.stop();
}


void Robot::stop()
{
  running = false;
}


Image* Robot::getLastImage()
{
  return image;
}

Image* Robot::getLastThresImage()
{
  return imageProcessor.getLastThresImage();
}


