#ifndef DEBUG_CONNECTION_H_INCLUDED
#define DEBUG_CONNECTION_H_INCLUDED


class FrameInfoHandler;


class DebugConnection
{
public:
  DebugConnection( void );
  ~DebugConnection( void );
  
  bool start( void );
  void stop( void );
  void writeDebugInfo( FrameInfoHandler* frameInfoHandler );
  
  
private:
  int sockfd;
  int newsockfd;
};



#endif