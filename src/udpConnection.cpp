#include "udpConnection.h"

#include <boost/bind.hpp>


using namespace boost;
using namespace boost::asio;



UdpConnection::UdpConnection( std::string host, int port )
  : host( host ),
    port( port ),
    ioRunning( false ),
    receiveBuffer2( buffer(receiveBuffer, udpBufferSize) ),
    udpThread( &UdpConnection::ioServer, this ),
    gotBall( false )
{
}



UdpConnection::~UdpConnection()
{
  udpThread.join();
  
  if (socket != NULL)
    delete socket;
}



void UdpConnection::ioServer()
{
  io_service ioService;
  
  socket = new udp::socket( ioService, udp::endpoint(udp::v4(), port) );
  remoteEndpoint = udp::endpoint( ip::address::from_string(host), port );

  receiveNext();
  
  ioRunning = true;
  
  ioService.run();
}


void UdpConnection::receiveNext()
{
  try {
    socket->async_receive_from(
      receiveBuffer2,
      endpoint,
      bind( &UdpConnection::onReceive, this, placeholders::error, placeholders::bytes_transferred )
    );
  } catch (std::exception& e) {
    std::cout << "- Communication receive error: " << e.what() << std::endl;
  }
}



void UdpConnection::onReceive( const boost::system::error_code& error, size_t bytesReceived )
{ 
  if ((!error || error == boost::asio::error::message_size) && bytesReceived > 0) {
    std::string msg = std::string(receiveBuffer, bytesReceived);

    /*
    if (msg.substr(0, 7) != "<speeds") {
      std::cout << "< " << msg << ", bytesReceived: " << bytesReceived << std::endl;
    }*/
    
    if (msg.substr(0, 5) == "<ball") {
      if (msg.at(6) == '0')
	gotBall = false;
      if (msg.at(6) == '1')
	gotBall = true;
    }
    
    if (ioRunning)
      receiveNext();
  }
}



void UdpConnection::send( std::string message )
{
  char sendBuffer[udpBufferSize];
  message += "\n";

  memcpy( sendBuffer, message.c_str(), message.size() );
  sendBuffer[message.size()] = 0;
  
  try {
    socket->send_to( buffer(sendBuffer, message.length()), remoteEndpoint );
  } catch (std::exception& e) {
    std::cout << "- Communication send error: " << e.what() << std::endl;
  }
}


void UdpConnection::close()
{
  ioRunning = false;
  
  if (socket != NULL) {
    try {
      boost::system::error_code ec;
      socket->shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
      socket->close();
    } catch (std::exception& e) {
      std::cout << "- Communication close error: " << e.what() << std::endl;
    }
  }
}



void UdpConnection::sendSpeeds( int flSpeed, int frSpeed, int rlSpeed, int rrSpeed, int dribblerSpeed )
{
  char speedStr[100];
  memset( speedStr, '\0', 100);
  sprintf( speedStr, "speeds:%d:%d:%d:%d:%d", flSpeed, frSpeed, rlSpeed, rrSpeed, dribblerSpeed );

  send( speedStr );
}



void UdpConnection::sendKick( int power )
{
  char sendStr[50];
  memset( sendStr, '\0', 50 );
  sprintf( sendStr, "kick:%d", power );
  
  send( sendStr );
}


void UdpConnection::sendCharge()
{
  send( "charge" );
}




