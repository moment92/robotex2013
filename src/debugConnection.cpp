#include "debugConnection.h"
#include "imgProc/frameInfoHandler.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <errno.h>




DebugConnection::DebugConnection()
{
}


DebugConnection::~DebugConnection()
{
}



bool DebugConnection::start()
{
  int portno = 47749;
  socklen_t clilen;
  struct sockaddr_in serv_addr, cli_addr;
  int flags;
  
  
  sockfd = socket( AF_INET, SOCK_STREAM, 0 );
  if (sockfd < 0)
    printf( "ERROR opening socket. %s\n", strerror(errno) );
  
  
  bzero( (char*) &serv_addr, sizeof(serv_addr) );
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons( portno );
  
  if ( bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0 )
    printf( "ERROR on binding. %s\n", strerror(errno) );

  listen( sockfd, 5 );
  
  clilen = sizeof( cli_addr );
  newsockfd = accept( sockfd, (struct sockaddr*) &cli_addr, &clilen );
  if (newsockfd < 0)
    printf( "ERROR on accept. %s\n", strerror(errno) );
  
  flags = fcntl( newsockfd, F_GETFL, NULL );
  fcntl( newsockfd, F_SETFL, flags | O_NONBLOCK );
}


void DebugConnection::stop()
{
  close( newsockfd );
  close( sockfd );
}



void DebugConnection::writeDebugInfo( FrameInfoHandler* frameInfoHandler )
{
  std::string sendStr = "frame ";
  std::vector<Blob> blobVec;
  char blobStr[50];
  Colors colorSendOrder[6] = { Green, White, Black, Blue, Yellow, Orange };
  
  for (int col=0; col<6; col++) {
    for (int i=1; i<=10; i++) {
      Blob blob = frameInfoHandler->getLargestBlob( colorSendOrder[col], i );
      if (blob.area > 0)
	blobVec.push_back( blob );
      else
	break;
    }
  }
  
  sendStr += std::to_string( blobVec.size() );
  
  for (int i=0; i<blobVec.size(); i++) {
    memset( blobStr, '\0', 50 );
    sprintf( blobStr,  " blob %d %d %d %d %d ", (int) blobVec[i].color, blobVec[i].x1, blobVec[i].y1, blobVec[i].x2, blobVec[i].y2 );
    sendStr += blobStr;
  }

  write( newsockfd, sendStr.c_str(), sendStr.size() );
}




