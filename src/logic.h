#ifndef LOGIC_H_INCLUDED
#define LOGIC_H_INCLUDED


#include "logic/movementController.h"
#include "logic/moveLogic.h"
#include "imgProc/coordinateTranslator.h"
#include "logic/tasks/task.h"

#include <stack>
#include <boost/shared_ptr.hpp>


class Dribbler;
class Coilgun;
class Wheel;
class FrameInfoHandler;


typedef boost::shared_ptr<Task> pTask;


class Logic
{
public:
  Logic( Dribbler* dribbler, Coilgun* coilgun, Wheel* wheelFL, Wheel* wheelFR, Wheel* wheelRL, Wheel* wheelRR );
  ~Logic( void );
  
  void work( FrameInfoHandler* frameInfoHandler );
  void setGateColor( Colors gateColor );
  
private:
  std::stack<pTask> taskList;
  
  MovementController moveCtrl;
  MoveLogic moveLogic;
  CoordinateTranslator translator;
  
  Dribbler* dribbler;
  Coilgun* coilgun;
  
  Colors gateColor;
};


#endif