#ifndef ROBOT_H_INCLUDED
#define ROBOT_H_INCLUDED

#include "wheel.h"
#include "dribbler.h"
#include "coilgun.h"
#include "camera.h"
#include "logic.h"
#include "imageProcessor.h"
#include "udpConnection.h"
#include "debugConnection.h"
#include "cameraConfig.h"


class Image;



enum GateColor
{
  YellowGate,
  BlueGate,
  NotChosen
};


const int imgWidth = 640;
const int imgHeight = 480;


class Robot
{
public:
  Robot( void );
  ~Robot( void );
  
  void run( Colors gateColor, bool motors, bool debug );
  void stop( void );
  Image* getLastImage( void );
  Image* getLastThresImage( void );
  
  
private:
  Camera camera;
  CameraConfig cameraConfig;
  
  Dribbler dribbler;
  Coilgun coilgun;
  
  Wheel wheelFL;
  Wheel wheelFR;
  Wheel wheelRL;
  Wheel wheelRR;
  
  Logic logic;
  ImageProcessor imageProcessor;
  
  UdpConnection udpConnection;
  
  Image* image;
  volatile bool running;
  
  DebugConnection debugConnection;
};


#endif