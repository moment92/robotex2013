#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

#include "cameraSettings.h"

#include <linux/videodev2.h>

#include <string>
#include <mutex>


typedef unsigned char uchar;

class CameraConf;


struct ImageBuffer {
  void* start;
  size_t length;
};


class Camera {
  
public:
  Camera( std::string devName_, CameraConf* config_ );
  ~Camera( void );
  
  int startCamera( void );
  void closeCamera( void );
  
  int openDevice( void );
  int closeDevice( void );
  int initDevice( void );
  int unInitDevice( void );
  int initMemoryMap( void );
  int startCapture( void );
  int stopCapture( void );
  
  uchar* readFrame( void );
  
  void getCameraControls( void );
  void parseCameraControl( v4l2_queryctrl& v4l2_ctrl, CameraSettings::CameraControl& camCtrl );
  int setCameraControl( CameraSettings::CameraControl& ctrl, int value );
  void getCameraOptions( void );
  void getSupportedOptions( void );
  int setFPS( int fps );
  int setResolution( int width, int height );
  int setPixelFormat( std::string name );
  
  
private:
  int findPixelFormat( unsigned int index );
  std::string fcc2s( unsigned int val );
  std::string getVarName( std::string name );
  
  
public:
  CameraSettings cameraSettings;
  
  
private:
  const char* devName;
  int fd;
  ImageBuffer* buffers;
  const int bufferCount;
  
  bool closed;
  volatile bool capturing;
  volatile bool initDone;
  volatile bool deviceOpen;
  
  std::mutex cameraMutex;
  
  CameraConf* config;
};


#endif