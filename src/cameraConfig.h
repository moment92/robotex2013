#ifndef CAMERA_CONFIG_H_INCLUDED
#define CAMERA_CONFIG_H_INCLUDED


#include "cameraSettings.h"

#include <string>
#include <vector>



class CameraConfig {
public:
  CameraConfig( void );
  ~CameraConfig( void );
  
  int readConfig( void );
  void parseConfigLine( std::string& line );
  void interpretContents( std::vector<std::string>& parts );
  void interpretCameraControl( std::string control, std::string value );
  
  void createConfig( void );
  void setDefaultConfig( void );
  void setDefaultCameraConfig( void );
  void applyCameraSettings( CameraSettings* settings );
  
  
private:
  std::string connectParts( std::vector<std::string>& parts, int startId );
  std::string int2str( int val );
  
  
public:
  std::string thresPath;
  std::string videoDevice;
  
  int mainWnd_x;
  int mainWnd_y;
  int confWnd_x;
  int confWnd_y;
  int wnd1_x;
  int wnd1_y;
  int wnd2_x;
  int wnd2_y;
  
  CameraConf cameraConfig;
};




#endif