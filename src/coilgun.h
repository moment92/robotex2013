#ifndef COILGUN_H_INCLUDED
#define COILGUN_H_INCLUDED


class Coilgun
{
public:
  Coilgun( void );
  ~Coilgun( void );
  
  void kick( int power );
  void noKick( void );
  
  
public:
  bool kickNow;
  int power;
};


#endif