#include "logic.h"
#include "wheel.h"
#include "dribbler.h"
#include "coilgun.h"
#include "imgProc/frameInfoHandler.h"

#include "logic/tasks/idleTask.h"
#include "logic/tasks/trackBallTask.h"
#include "logic/tasks/driveCircleTask.h"
#include "logic/tasks/attackBallTask.h"


#include <cstdio>
#include <cmath>



Logic::Logic(Dribbler* dribbler, Coilgun* coilgun, Wheel* wheelFL, Wheel* wheelFR, Wheel* wheelRL, Wheel* wheelRR)
  : dribbler( dribbler ),
    coilgun( coilgun ),
    moveCtrl( wheelFL, wheelFR, wheelRL, wheelRR ),
    moveLogic( &moveCtrl, &translator )
{
  translator.loadConf( "cameraConf" );
  taskList.push( pTask(new IdleTask) );
}



Logic::~Logic()
{
}



void Logic::work( FrameInfoHandler* frameInfoHandler )
{  
  pTask task = taskList.top();
  TaskResponse* response = task->process( frameInfoHandler, &moveLogic );
  if (task->endCondition()) {
    taskList.pop();
    printf("%d end\n", response->taskType);
  }
  
  
  switch (response->taskType)
  {
    case Idle:
      if (response->ballSeen)
	taskList.push( pTask(new TrackBallTask) );
      break;
      
    case TrackBall:
      if (response->ballSeen && response->ballDistance < 50)
	taskList.push( pTask(new driveCircleTask) );
      break;
      
    case DriveCircle:
      if (response->goalAlligned)
	taskList.push( pTask(new AttackBallTask) );
      break;
  }
}



void Logic::setGateColor( Colors gateColor )
{
  this->gateColor = gateColor;
  moveLogic.setGateColor( gateColor );
}







